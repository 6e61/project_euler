"""
Problem 1
Multiples of 3 and 5

If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9.
The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.
"""


if __name__ == '__main__':
    while True:
        user_input = input('\n--< Find sum of all multiples of 3 and 5'
                           'below (or q to quit): ')
        if user_input == 'q':
            break
        multiples_sum = sum(
            (num for num in range(1, (int(user_input)))
             if num % 3 == 0 or num % 5 == 0)
        )
        print(f'--> Sum is: {multiples_sum}\n')
