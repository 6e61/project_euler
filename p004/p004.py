"""
Problem 4
Largest palindrome product

A palindromic number reads the same both ways.
The largest palindrome made from the product of two 2-digit numbers is
9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""


def largest_palindrome_product(num_of_digits):
    if num_of_digits < 2:
        return None
    lst = []
    rng = (10 ** (num_of_digits - 1), 10 ** num_of_digits)
    for i in range(*rng):
        for j in range(*rng):
            prod = str(i * j)
            if prod[0:num_of_digits] == prod[:num_of_digits-1:-1]:
                lst.append(dict(zip(['i', 'j', 'prod'],[i, j, int(prod)])))
    return (max([(e['prod'], it) for it, e in enumerate(lst)]))[0]


if __name__ == '__main__':
    pass
