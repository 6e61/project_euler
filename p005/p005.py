"""
Problem 5
Smallest multiple

2520 is the smallest number that can be divided by each of the numbers from
1  to 10 without any remainder.
What is the smallest positive number that is evenly divisible by all of the
numbers from 1 to 20?

===
algorithm:
1. generate i*20 # divisible by: 1, 2, 4, 5, 10, 20
1. generate i*40 # divisible by: 1, 2, 4, 5, 8, 10, 20
1. generate i*80 # divisible by: 1, 2, 4, 5, 8, 10, 16, 20
2. sum of digits % 3 == 0 # divisible by: 3, 6, 9, 12, 15, 18
3. s = a1 - a2 + a3 - ... # divisible by: 7, 11 ,13 (14 promo)
4. check 17 + 19 with different algorithm
"""


def smallest_multiple():
    num = 80
    while True:
        ss = sum(list(map(int, str(num))))
        #print(ss)
        if not ss % 3 and not ss % 9:
            if not 1 in [1 if num % div else 0 for div in [7, 11, 13, 17, 19]]:
                return num
        num += 80


if __name__ == '__main__':
    print(smallest_multiple())
