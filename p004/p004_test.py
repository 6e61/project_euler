import pytest
from p004 import largest_palindrome_product


@pytest.mark.parametrize('digits', (
    0, 1
))
def test_no_palindrome(digits):
    assert largest_palindrome_product(digits) is None


@pytest.mark.parametrize('digits, palindrome', (
    (2, 9009),
    (3, 906609),
    #(4, 99000099),
))
def test_largest_palindrome(digits, palindrome):
    assert largest_palindrome_product(digits) == palindrome
