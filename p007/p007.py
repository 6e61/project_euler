"""
Problem 7

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""
from datetime import datetime
from math import sqrt


def is_prime(number):
    if number <= 1:
        return False
    elif number in (2, 3):
        return True
    else:
        for i in range(2, int(sqrt(number)) + 1):
            if not number % i:
                return False
    return True


def find_nth_prime_number(searched_index):
    index, value = 1, 1
    while index <= searched_index:
        value += 1
        if is_prime(value):
            index += 1
    return value


start_timestamp = datetime.now()
print(find_nth_prime_number(10_001))
print(datetime.now() - start_timestamp)