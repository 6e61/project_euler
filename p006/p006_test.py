import pytest
from p006 import sum_of_squares, square_of_sum


@pytest.mark.parametrize("number, result", (
    (2, 5),
    (3, 14),
    (10, 385),
))
def test_sum_of_squares(number, result):
    assert sum_of_squares(number) == result


@pytest.mark.parametrize("number, result", (
    (2, 9),
    (3, 36),
    (10, 3025),
))
def test_square_of_sum(number, result):
    assert square_of_sum(number) == result
